#ifndef DEF_H
#define DEF_H

#include <Arduino.h>

// ---  byte operations --- //

#define _GB(byte, mask) (byte &   mask)
#define _SB(byte, mask) (byte |=  mask)
#define _CB(byte, mask) (byte &= ~mask)
#define _TB(byte, mask) (byte ^=  mask)


// ---  pwm range --- //

#define CH_IN_MIN (uint16_t)((float)(F_CPU) * 0.001)   // 1ms
#define CH_IN_MED (uint16_t)((float)(F_CPU) * 0.0015)  // 1,5ms
#define CH_IN_MAX (uint16_t)((float)(F_CPU) * 0.002)   // 2ms


// --- pinout --- //

#define CH2_IN PD4
#define CH3_IN PD5
#define CH4_IN PD6
#define CH5_IN PD7
#define CH_IN_D (_BV(CH2_IN) | _BV(CH3_IN) | _BV(CH4_IN) | _BV(CH5_IN))  // (D4/PD4 | D5/PD5 | D6/PD6 | D7/PD7)

#define CH6_IN PB2
#define CH7_IN PB3
#define CH8_IN PB4
#define CH_IN_B (_BV(CH6_IN) | _BV(CH7_IN) | _BV(CH8_IN))  // (10/PB2 | 11/PB3 | 12/PB4)

#define CH1_OUT PB0
#define CH2_OUT PB1
#define CH_OUT (_BV(CH1_OUT) | _BV(CH2_OUT)) // (D8/PB0 | D9/PB1)

#define INPUT_CH_COUNT_D 4
#define INPUT_CH_COUNT_B 3

#define INPUT_CH_COUNT INPUT_CH_COUNT_D + INPUT_CH_COUNT_B

#define OUTPUT_CH_COUNT 2

// --- stabilization modes --- //

enum StabModes{
  BYPASS_MODE,    // mode where incoming pwm is transfered directly to output
  ROLL_STAB_MODE, // plane will try to return to neutral roll
  FULL_STAB_MODE, // plane will try to return to neutrall roll and pitch
  EMERGENCY_MODE  // in case of signal loss plane will start to circle around
};

#endif
