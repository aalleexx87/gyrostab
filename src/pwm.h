#include <Arduino.h>
#include "def.h"

#define CH_IN_MIN_TO_MED (( CH_IN_MIN + CH_IN_MED ) >> 1)
#define CH_IN_MED_TO_MAX (( CH_IN_MED + CH_IN_MAX ) >> 1)

volatile uint8_t pulsein_old_pind_state = 0;
volatile uint8_t pulsein_old_pinb_state = 0;
volatile uint16_t pulsein_start_times[INPUT_CH_COUNT];

volatile uint16_t pulsein_widths[INPUT_CH_COUNT];
volatile uint16_t pulseout_widths[OUTPUT_CH_COUNT];

volatile StabModes mode = BYPASS_MODE;

ISR(TIMER1_COMPA_vect)
{
  _CB(TIMSK1, _BV(OCIE1A));
  _CB(PORTB, _BV(CH1_OUT));
}

ISR(TIMER1_COMPB_vect)
{
  _CB(TIMSK1, _BV(OCIE1B));
  _CB(PORTB, _BV(CH2_OUT));
}

ISR (PCINT2_vect)
{
  volatile uint8_t sreg = SREG;
  cli();

  volatile uint16_t time = TCNT1;
  volatile uint8_t pind_changed_mask = PIND ^ pulsein_old_pind_state;
  pulsein_old_pind_state = PIND;

  for (volatile size_t ch = 0; ch < INPUT_CH_COUNT_D; ch++)
  {
    if ( _GB(pind_changed_mask, _BV(CH2_IN + ch)) )
    {
      if( _GB(pulsein_old_pind_state, _BV(CH2_IN + ch)) )
      {
        pulsein_start_times[ch] = time;
      }
      else
      {
        pulsein_widths[ch] = time - pulsein_start_times[ch];

        switch(ch)
        {
        case 0:
          if(mode == BYPASS_MODE)
          {
            pulseout_widths[0] = pulsein_widths[0];
          }

          OCR1A = TCNT1 + pulseout_widths[0];
          _SB(PORTB, _BV(CH1_OUT));

          _SB(TIFR1, _BV(OCF1A));
          _SB(TIMSK1, _BV(OCIE1A));
        break;

        case 1:
          if(mode == BYPASS_MODE)
          {
            pulseout_widths[1] = pulsein_widths[1];
          }

          OCR1B = TCNT1 + pulseout_widths[1];
          _SB(PORTB, _BV(CH2_OUT));

          _SB(TIFR1, _BV(OCF1B));
          _SB(TIMSK1, _BV(OCIE1B));
        break;

        case 2:
        break;

        case 3:
        break;
        }
      }
    }
  }
  SREG = sreg;
}

ISR (PCINT0_vect)
{
  volatile uint8_t sreg = SREG;
  cli();

  volatile uint16_t time = TCNT1;
  volatile uint8_t pinb_changed_mask = PINB ^ pulsein_old_pinb_state;
  pulsein_old_pinb_state = PINB;

  for (volatile size_t ch = 0; ch < INPUT_CH_COUNT_B; ch++)
  {
    if ( _GB(pinb_changed_mask, _BV(CH6_IN + ch)) )
    {
      if( _GB(pulsein_old_pinb_state, _BV(CH6_IN + ch)) )
      {
        pulsein_start_times[INPUT_CH_COUNT_D + ch] = time;
      }
      else
      {
        pulsein_widths[INPUT_CH_COUNT_D + ch] = time - pulsein_start_times[INPUT_CH_COUNT_D + ch];

        switch(ch)
        {
          case 0:
          break;

          case 1:
            if(mode != EMERGENCY_MODE)
            {
              if(pulsein_widths[5] < (CH_IN_MIN_TO_MED))
              {
                mode = BYPASS_MODE;
              }
              else if (pulsein_widths[5] > CH_IN_MED_TO_MAX)
              {
                mode = FULL_STAB_MODE;
              }
              else
              {
                mode = ROLL_STAB_MODE;
              }
            }
          break;

          case 2:
          break;
        }
      }
    }
  }
  SREG = sreg;
}

void timersSetup() {
  TCCR1A = 0x00;
  TCCR1B = 0x01;
}

void inputChannelsPinsSetup() {
  _CB(DDRD, CH_IN_D); // ~ (D4/PD4 | D5/PD5 | D6/PD6 | D7/PD7)
  _SB(PORTD, CH_IN_D);

  _CB(DDRB, CH_IN_B); // ~ (10/PB2 | 11/PB3 | 12/PB4)
  _SB(PORTB, CH_IN_B);

  _SB(PCICR, (_BV(PCIE2) | _BV(PCIE0)));
  _SB(PCMSK0, CH_IN_B);
  _SB(PCMSK2, CH_IN_D);

  pulsein_old_pind_state = PIND;
  pulsein_old_pinb_state = PINB;
  sei();
}

void outputChannelsPinsSetup() {
  _CB(PORTB, CH_OUT); // ~ (D8/PB0 | D9/PB1)
  _SB(DDRB, CH_OUT);
}
