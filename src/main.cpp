#include <avr/wdt.h>
#include "pwm.h"
#include "gyro.h"


void stabilise();
float normalizeRotation(float);

void setup()
{
    Serial.begin(115200);
    timersSetup();
    outputChannelsPinsSetup();
    inputChannelsPinsSetup();
    gyroSetup();

    for (volatile size_t ch = 0; ch < INPUT_CH_COUNT; ch++)
    {
      pulsein_widths[ch] = CH_IN_MED;
    }

    for (volatile size_t ch = 0; ch < OUTPUT_CH_COUNT; ch++)
    {
      pulseout_widths[ch] = CH_IN_MED;
    }

    delay(50);

    if(_GB(MCUSR, _BV(WDRF)))
    {
      calibration_count = CALIBRATION_LOOPS + 1;
    }

    wdt_enable(WDTO_250MS);
}

void loop()
{
  wdt_reset();

  if(gyroRead() == true) {
    stabilise();
  }
}

void stabilise()
{
  float gyro_divider = 100.0;

  int16_t servo_correction = (int16_t)(pulsein_widths[3] >> 2) - (int16_t)(CH_IN_MED >> 2);

  float roll_gain = max( (float)((int16_t)(pulsein_widths[2] >> 1) - (int16_t)(CH_IN_MIN >> 1)), 0.0);
  float pitch_gain = max( (float)((int16_t)(pulsein_widths[4] >> 1) - (int16_t)(CH_IN_MIN >> 1)), 0.0);

  float roll = (normalizeRotation(ypr[2] + ypr_calibration[2]));
  float pitch = (normalizeRotation(ypr[1] + ypr_calibration[1]));

  if(mode == ROLL_STAB_MODE)
  {
    pulseout_widths[0] = min(max(pulsein_widths[0] + (int16_t)floor(roll_gain * roll) + servo_correction, CH_IN_MIN), CH_IN_MAX);
    pulseout_widths[1] = min(max(pulsein_widths[1] + (int16_t)floor(roll_gain * roll) + servo_correction, CH_IN_MIN), CH_IN_MAX);
  }
  else if(mode == FULL_STAB_MODE)
  {
    //pulseout_widths[0] = min(max(pulsein_widths[0] + (int16_t)floor(roll_gain * roll) + (int16_t)floor(pitch_gain * pitch) + servo_correction, CH_IN_MIN), CH_IN_MAX);
    //pulseout_widths[1] = min(max(pulsein_widths[1] + (int16_t)floor(roll_gain * roll) - (int16_t)floor(pitch_gain * pitch) + servo_correction, CH_IN_MIN), CH_IN_MAX);
    pulseout_widths[0] = min(max(pulsein_widths[0] + (int16_t)floor(roll_gain * (float)(gyro[0])/gyro_divider) + (int16_t)floor(pitch_gain * (float)(gyro[1])/gyro_divider) + servo_correction, CH_IN_MIN), CH_IN_MAX);
    pulseout_widths[1] = min(max(pulsein_widths[1] + (int16_t)floor(roll_gain * (float)(gyro[0])/gyro_divider) - (int16_t)floor(pitch_gain * (float)(gyro[1])/gyro_divider) + servo_correction, CH_IN_MIN), CH_IN_MAX);
  }
}

float normalizeRotation(float rotation)
{
    if(rotation > M_PI) {
      rotation -= 2*M_PI;
    }
    else if(rotation < - M_PI)
    {
      rotation += 2*M_PI;
    }
    return rotation;
}
